require 'spec_helper'

describe Integer do
  describe 'to_s' do
    it 'meets the performance metrics when running 1 time' do
      expect{5.to_s}.to run_in_less_than(1.second)
    end

    it 'meets the performance metrics when running 500 times' do
      expect{5.to_s}.to run(500).times_in_less_than(5.seconds)
    end
  end
end
