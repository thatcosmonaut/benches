# Benches

Benches defines a simple rspec matcher that allows you to create benchmarking specs for your Ruby code.

## Usage

To include the matchers in your specs, set up your `spec_helper.rb` like so:
```ruby
  require 'benches'
  # other requirements

  RSpec.configure do |config|
    config.include Benches::Matchers
    #other configuration code
  end
```

The run_in_less_than matcher expects a block and checks if the block runs in less than a certain amount of time.

```ruby
  describe Integer
    describe 'to_s' do
      it 'meets the performance metrics when running 1 time' do
        expect{5.to_s}.to run_in_less_than(1.second)
      end
    end
  end
```

You can also specify that a block should run a certain number of times in less than a certain duration.

```ruby
describe Integer do
  describe 'to_s' do
    it 'meets the performance metrics' do
      expect{5.to_s}.to run(500).times_in_less_than(5.seconds)
    end
  end
end
```

The duration specified should be an ActiveSupport::Duration object, like `5.seconds` or `1.hour`.

If the benchmark runs within the allotted time, the test will pass.
Otherwise it will fail.

## Testing

Simply run `rspec` to run the sample spec.

## Credits

All code (c) Evan Hemsley 2014
