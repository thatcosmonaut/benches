# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "benches/version"

Gem::Specification.new do |s|
  s.name        = "benches"
  s.version     = Benches::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Evan Hemsley"]
  s.email       = ["evan.hemsley@gmail.com"]
  s.homepage    = 'http://gitlab.com/ehemsley/benches'
  s.summary     = 'A simple rspec matcher for testing performance metrics'
  s.description = 'Benches defines a simple rspec matcher that allows you to create benchmarking specs for your Ruby code.'

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.add_dependency 'rspec', '~> 2.14', '>= 2.14.1'
  s.add_dependency 'activesupport', '~> 4.0', '>= 4.0.3'

  s.license       = 'MIT'
end
